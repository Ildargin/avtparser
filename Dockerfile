FROM buildkite/puppeteer

WORKDIR /parser

COPY package.json /parser/package.json

RUN npm install

COPY . /parser

CMD ["npm", "start"]