require("dotenv").config();
const { readData, writeData } = require("./utils/data")
const getFlats = require("./utils/getFlats")
const sendNotification = require("./utils/sendNotifications")
const path = require("path");
const FLATS_PATH = path.resolve("flats.json");
const USERS_PATH = path.resolve("users.json");
const { CronJob } = require("cron");
const EVERY_5_MINUTE = "*/5 * * * *";
const EVERY_MINUTE = "*/1 * * * *";
const { Telegraf } = require('telegraf')
const BOT_TOKEN = process.env.BOT_TOKEN;
const bot = new Telegraf(process.env.BOT_TOKEN);


const job = new CronJob(
    EVERY_MINUTE,
    async () => {
        let oldFlats;
        let users;
        let newFlats;
        console.log("start fetching new flats...");
        try {
            newFlats = await getFlats();
            console.log("new flats fetched: ", newFlats.length);
        } catch (e) {
            console.log('Parsing error', e)
        }
        try {
            console.log("starting reading flats and users from memory...");
            oldFlats = readData(FLATS_PATH);
            users = readData(USERS_PATH)
        } catch (e) {
            console.log("Read error, starting writing data...", e);
            writeData(newFlats, FLATS_PATH);
            return;
        }
        if (Array.isArray(oldFlats) && Array.isArray(newFlats)) {
            console.log("starting filtering new flats...");
            const newFlatsLinks = newFlats.map((obj) => obj.link);
            const oldFlatsLinks = oldFlats.map((obj) => obj.link);
            const diffFlatsLinks = newFlatsLinks.filter(
                (link) => !oldFlatsLinks.includes(link)
            );
            const newData = newFlats.filter((flat) =>
                diffFlatsLinks.includes(flat.link)
            );
            console.log('newFlatsCount:', newData.length)
            writeData(newFlats, FLATS_PATH);
            users.forEach(id =>{
                sendNotification(bot, id, newData);
            })
            
        }
    },
    null,
    true,
    "America/Los_Angeles"
);

bot.start(async (ctx) => {
    let message = 'Notification\'s on'
    let id = ctx.chat.id
    try {
        const users = readData(USERS_PATH)
        if (!users.includes(id)) {
            users.push(id)
        }
        writeData(users, USERS_PATH)
    } catch (e) {
        console.log(e)
        message = 'Somthing went wrong'
    }
    ctx.reply(message)
})

bot.launch()
job.start()