const fs = require('fs')

const writeData = (data, filename) => {
    const stringData = JSON.stringify(data);
    if (stringData) {
        fs.writeFile(filename, stringData, (err) => {
            if (err) {
                return err;
            }
            console.log("save successful");
        });
    }
};

const readData = (filename) => {
    const data = fs.readFileSync(filename, "utf-8");
    const dataObject = JSON.parse(data);
    return dataObject;
}

module.exports = {
    writeData,
    readData
}