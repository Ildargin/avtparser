const puppeteer = require("puppeteer");
const AVITO_LINK = process.env.AVITO_LINK;

module.exports = async () => {
    const browser = await puppeteer.launch({
        headless: true,
        args: [
            '--no-sandbox',
            '--disable-gpu',
        ]
    });
    const page = await browser.newPage();
    await page.goto(AVITO_LINK, {'timeout': 10000, 'waitUntil':'load'});
    await page.content();
    const result = await page.evaluate(() => {
        document.querySelectorAll("div[class*=vip]").forEach(e => e.remove())
        const flats = document.querySelectorAll("[data-marker='item']");
        console.log("parser:flats", flats.length)
        let data = [];
        flats.forEach((item) => {
            const date = item.querySelector("[data-marker='item-date']");
            const linkWithName = item.querySelector("[data-marker='item-title']");
            const description = item.querySelector("meta[itemprop='description']");
            data.push({
                name: linkWithName.innerText,
                link: linkWithName.href,
                date: date.innerText,
                description: String(description.content),
            });
        });
        return Promise.resolve(data);

    });
    await browser.close();
    return result;
};