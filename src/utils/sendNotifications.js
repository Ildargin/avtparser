module.exports =  (bot, id, data) => {
    // data = [] {
    //  name: linkWithName.innerText,
    //  link: linkWithName.href,
    //  date: date.innerText,
    //  description: String(description.content)
    //}
    if (data.length === 0 ) {
        console.log('New data is empty')
    }
    data.forEach(async (el) => {
        const beauty = `\n${el.name}\n${el.link}\n${el.description}\n${el.date}` 
        await bot.telegram.sendMessage(id, beauty)
    })
}